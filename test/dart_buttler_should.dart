import 'package:bididi/feature.dart';
import 'package:bididi/scenario.dart';
import 'package:dart_buttler/buttler.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fs_shim/fs_io.dart';

const BUTTLER_FILE = "buttler";

void main() async {
  Feature('Download Rust Buttler binary', () {
    Scenario("Success Download", (scenario) {
      Buttler buttler;
      scenario
        ..given("I want to get Buttler", () async {
          buttler = Buttler(path: "lib/");
        })
        ..when("I use DartButtler", () async {
          await buttler.run();
        })
        ..then("Then I have the Buttler binary on my filesystem", () async {
          final weHaveAButtlerFile = await fileSystemIo.file(BUTTLER_FILE).exists();
          expect(weHaveAButtlerFile, isTrue);
          await removeFileAndTestThatItIsGone();
        });
    });
  });
}

removeFileAndTestThatItIsGone() async {
  await fileSystemIo.file(BUTTLER_FILE).delete();
  final isItStillThere = await fileSystemIo.file(BUTTLER_FILE).exists();
  expect(isItStillThere, isFalse);
}
