//git archive --remote=git@gitlab.com:mobiplay/buttler.git HEAD:target/release buttler | tar -x
import 'dart:io';

import 'package:dart_extras/dart_extras.dart';
import 'package:process_run/shell.dart';

const GET_BUTTLER_SH = "get_buttler.sh";

class Buttler {
  String path;

  Buttler({String path}) : this.path = path;

  run() async {
    print("Dart trying to get Buttler:");
    final _path = path ?? await getPath(this.path ?? GET_BUTTLER_SH);
    final shell = Shell();
    await shell.run('''sh $_path''');
    final weHaveAButtleFile = FileSystemEntity.typeSync("buttler") != FileSystemEntityType.notFound;
    print("Do we have a buttler file ==> $weHaveAButtleFile ");
    if (weHaveAButtleFile) {
      await shell.run('''./buttler hello''');
    } else {
      print("Something has gone wrong we don't seem have been able to download buttler");
      await shell.run('''ls -a''');
      await shell.run("exit");
    }
  }

  Future<String> getPath(String filepath) async {
    var uri = await ResourceResolver.resolveUri("package:dart_buttler/$filepath");
    return uri.toFilePath();
  }
}
